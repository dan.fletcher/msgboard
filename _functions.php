<?php


function connectDB()
{

	// define our variable so they can be used in other _functions
	global $collection_thread, $collection_auth;

	// connect
	$m = new MongoClient();

	// select a database
	$db = $m->sjp;

	// select a collection (analogous to a relational database's table)

	// array(mcatagory,scatagory,topic,post(array("dave","24/5/17","my comments are"),,,,)
	// this is a main catagory, sub catagory, topic title and posts
	// there will be one array inside posts for each message
	$collection_thread = $db->sjp_thread;   //Chat messages
	// and a sticky note
	// array("sticky" => true, "note" => "my sticky note")

	// array(email,firstname,lastname,hash)  
	// passwords are stored as md5 hash
	$collection_auth = $db->sjp_auth;  //SJP Site Maintainers
}

	

function newUser($email, $firstname, $lastname, $password) 
{
	// callin shared variable
	global $collection_auth;

	// validate all data
	field_validator("password", $password, "alphanumeric_space", 4, 32);
	field_validator("firstname", $firstname, "alpha_space", 2, 30);
	field_validator("lastname", $lastname, "alphp_space", 2, 30);
	field_validator("email", $email, "email", 6, 50);

	// show validation errors
	if($messages)
	{
        	foreach($messages as $line)
        	{
                	print "<p><font color=\"red\">".$line."</font><p>";
        	}
	} 
	else
	{	
		// we only store in hash so convert
		$hash = md5($password);	

	        // make array of record
	        $document = array(
	                "email" => $email,
	                "firstname" => $firstname,
			"lastname" => $lastname,
			"hash" => $hash,
			"notify" => true
	        );

		// add record
	        $collection_auth->insert($document);	
		print $firstname." ".$lastname." has been inserted";
		
		
	}	
}

function groupAdd($id, $groupname)
{
        // callin shared variable
        global $collection_auth;
        field_validator("UserID", $id, "alphanumeric", 2, 20);
        field_validator("Group Name", $groupname, "alphanumeric_space", 2, 20);

	$groupname = strtolower($groupname);	
        // update the record
        $collection_auth->update(array('_id' => new MongoID($id)), array('$push' => array("permission" => $groupname)));
}


function userEdit($id)
{
        // callin shared variable
        global $collection_auth;

        // find the account ID
        $cursor = $collection_auth->findOne(array('_id' => new MongoID($id)));

	if ($cursor['notify'] == "on"){ $checked = "checked"; }
	
	print "<form action=".$_SERVER["PHP_SELF"]." method=\"post\">";
	print "<input type=\"hidden\" name=\"id\" value=\"".$id."\">";
	print "Email: <input type=\"text\" name=\"email\" value=\"".$cursor['email']."\"><br>";
        print "Firstname: <input type=\"text\" name=\"firstname\" value=\"".$cursor['firstname']."\"><br>";
	print "Lastname: <input type=\"text\" name=\"lastname\" value=\"".$cursor['lastname']."\"><br>";
	print "Password: <input type=\"password\" name=\"password\" value=\"".$cursor['password']."\"> (blank leave unchanged)<br>";
	print "Email notify posts: <input type=\"checkbox\" name=\"notify\" ".$checked."><br>";
	print "<input type=\"submit\">";
	print "</form>";

}


function groupList($id)
{
        // callin shared variable
        global $collection_auth;
        field_validator("UserID", $id, "alphanumeric", 2, 20);
        
        $groupname = strtolower($groupname);
        // update the record
        $cursor = $collection_auth->findOne(array('_id' => new MongoID($id)));

	$permissions = $cursor["permission"];
	
	$return = "<p>Users existing permissions</p>\n";
	foreach ($permissions as $document)
	{
                {
                        $return = $return.$document."<br>\n" ;
                }

        }

	return $return; 

}


function pollSave($id, $choice)
{
	// callin chared variable
	global $collection_thread;

	$collection_thread->update(array('_id' => new MongoID($id)), array('$set' => array("responses" => array("name" => $_SESSION["name"], "choice" => $choice))));
}


function userSave($id, $email, $firstname, $lastname, $password, $notify)
{
        // callin chared variable
        global $collection_auth;

        // validate all data
        field_validator("password", $password, "alphanumeric_space", 0, 32);
        field_validator("firstname", $firstname, "alpha_space", 2, 30);
        field_validator("lastname", $lastname, "alphp_space", 2, 30);
        field_validator("email", $email, "email", 6, 50);
        field_validator("notify", $notify, "alpha", 0, 2);

	if ($notify == "on" ){$notify="on";}else{$notify="off";}
        // show validation errors
        if($messages)
        {
                foreach($messages as $line)
                {
                        print "<p><font color=\"red\">".$line."</font><p>";
                }
        }
        else
        {
                // we only store in hash so convert
                if ($password != "") 
		{
			$hash = md5($password);

                	// make array of record
                	$document = array(
                        	"email" => $email,
                        	"firstname" => $firstname,
                        	"lastname" => $lastname,
                        	"hash" => $hash,
                        	"notify" => $notify
                	);
		}
		else
		{
                        // make array of record
                        $document = array(
                                "email" => $email,
                                "firstname" => $firstname,
                                "lastname" => $lastname,
                                "notify" => $notify
                        );
		}

        // update record
	
        $collection_auth->update(array('_id' => new MongoID($id)), array('$set' => $document));
	}
}


function newThread($mcatagory, $scatagory, $topic)
{

        // callin shared variable
        global $collection_thread;

        // validate all data
        field_validator("MainCatagory", $mcatagory, "alphanumeric_space", 2, 20);
        field_validator("SubCatagory", $scatagory, "alphanumeric_space", 2, 20);
        field_validator("Topic", $topic, "alphanumeric_space", 2, 30);

	$timestamp=round(microtime(true) * 100);

        // start a new thread
        $document = array(
                "maincatagory" => $mcatagory,
                "subcatagory" => $scatagory,
                "topic" => $topic,
		"timestamp" => $timestamp		
        );
	
        $collection_thread->insert($document);
	return $document['_id'];
}

function stickyNew($note)
{

        // callin shared variable
        global $collection_thread;

        // validate all data
        field_validator("Note", $note, "alphanumeric_space", 2, 50);

        // start a new thread
        $document = array(
                "sticky" => true,
                "note" => $note
        );

        $collection_thread->insert($document);
}


function threadPost($id, $post)
{

        // callin shared variable
        global $collection_thread;

        // validate all data
        field_validator("id", $id, "alphanumeric", 24, 24);
        field_validator("post", $post, "alphanumeric_space", 2, 2000);
	$today = date("d.m.y"); 
	$name = $_SESSION["name"];

	// use an incrementing number (actually time) to find new messages
	$timestamp = round(microtime(true) * 100);

        // update the record
        $collection_thread->update(array('_id' => new MongoID($id)), array('$push' => array("post" => array("name" => $name, "date" => $today, "msg" => $post))));
	$collection_thread->update(array('_id' => new MongoID($id)), array('$set' => array("timestamp" => $timestamp)));
	emailPost($id, $post);
}

function emailPost($id, $post)
{
        global $collection_auth;
	global $collection_thread;

	// find post subject
        $cursor = $collection_thread->findOne(array('_id' => new MongoID($id)));
	$subject = $cursor["topic"];
	
	// set mail header
	$header  = "From: ".$_SESSION['name']." <".$_SESSION['email']."> \r\n";
	$header .= "Reply-To: post@stjamesparkapartments.co.uk \r\n";
	$header .= "X-Mailer: PHP/".phpversion();
	
        // find all users
        $cursor = $collection_auth->find(array("notify" => "on"));

        foreach($cursor as $auth)
        {
                mail($auth['email'], $subject."  [".$id."]", $post, $header);
        }
}


function authDelete($id)
{
        // callin shared variable
        global $collection_auth;

        // validate all data
        field_validator("id", $id, "alphanumeric", 24, 24);

	// remove entry
        $collection_auth->remove(array('_id' => new MongoID($id)));
}


function stickyDelete($id)
{
        // callin shared variable
        global $collection_thread;

        // validate all data
        field_validator("id", $id, "alphanumeric", 24, 24);

        // remove entry
        $collection_thread->remove(array('_id' => new MongoID($id)));
}


/******************************************************\
 * Function Name : displayErrors($messages)
 *
 * Task : display a list of errors
 *
 * Arguments : array $messages
 *
 * Returns : none
 *
 \******************************************************/
function displayErrors($messages) {
	/*
	Error Handling _functions:
	An error handling function is useful to have in any project.

	This particular function takes an array of messages, and
	for each message displays it in a list using HTML
	<ul><li></li></ul> tags.
	*/
	print("<b>There were problems with the previous action.  Following is a list of the error messages generated:</b>\n<ul>\n");

	foreach($messages as $msg){
		print("<li>$msg</li>\n");
	}
	print("</ul>\n");
} // end eunc displayErrors($messages)


function threadList($main,$sub)
{
        // callin shared variable
        global $collection_thread;
	
	$return = "";
	
	if ($sub != "" )
	{
        	print "<p><a class=\"thread_list\" href=\"thread_new.php?main=".$main."&sub=".$sub."\">New Conversation</a></p>\n";
		print "<p><a class=\"thread_list\" href=\"poll_new.php?main=".$main."&sub=".$sub."\">New Poll</a></p>\n";
		
		// if subcatagory is provided return a list of threads
                $cursor = $collection_thread->find(array("maincatagory" => $main, "subcatagory" => $sub));
		foreach ($cursor as $document)
		{
			$id=trim($document["_id"]);
                	$topic=$document["topic"];
			
			if ($document["poll"])
			{
				print "<a class=\"thread_list\" href=\"poll.php?id=".$id."\">VOTE :: ".$topic."</a>";
			}
			else
			{
				print "<a class=\"thread_list\" href=\"thread.php?id=".$id."\">".$topic."</a>";
		        }

			$cookie = $_COOKIE[$id];
	
			if ($document["timestamp"] > $_COOKIE[$id])
		        {
                		print " * ";
        		}
	
			print " <br>\n";
		}
	}
	elseif ($main != "")
	{
                // if main catagory is provided return only list of sub catagories
                $cursor = $collection_thread->distinct( "subcatagory", array("maincatagory" => $main) );
                foreach ($cursor as $document)
                {
                	print "<a class=\"thread_list\" href=\"".$_SERVER["PHP_SELF"]."?main=".$main."&sub=".$document."\">".$document."</a><br>\n";
		}

	}
	else
	{
        	// return a list of all catagories
        	$cursor = $collection_thread->distinct( "maincatagory" );
		foreach ($cursor as $document) 
		{
			print "<a class=\"thread_list\" href=\"".$_SERVER["PHP_SELF"]."?main=".$document."\">".$document."</a><br>\n";
		}

	}
}


function stickyList()
{
        // callin shared variable
        global $collection_thread;

        // find everything in the collection
        $cursor = $collection_thread->find(array("sticky" => array( '$exists' => true )));

        // iterate through the results
	$return = "<p>\n";
        foreach ($cursor as $document) 
	{
                $note = $document["note"];
                $return=$return."<css class=\"sticky_banner\">".$note."</css><br>\n";
        };
        print $return."</p>\n";
}

function stickyListForm()
{
        // callin shared variable
        global $collection_thread;

        // find everything in the collection
        $cursor = $collection_thread->find(array("sticky" => true));

        // iterate through the results

        foreach ($cursor as $document)
        {
                $note = $document["note"];
                $return=$return.$note."<br>\n";
                Print "<form action=".$_SERVER["PHP_SELF"]." method=\"post\">";
		print $note;
                Print "<input type=\"hidden\" name=\"id\" value=\"".$document['_id']."\">";
                Print "<input type=\"submit\" value=\"Delete\"></form>";
        };
}
 

function threadPosts($id)
{
	// callin shared variables
	global $collection_thread;

	// check its a proper id
	field_validator("id", $id, "alphanumeric", 24, 24);

        // return the users password hash
	$cursor = $collection_thread->findOne(array('_id' => new MongoID($id)));

	print "<p class=\"thread_list\">".$cursor["topic"]."</p>\n\n";
	// take the posts from the complete array
	$post = array_reverse($cursor["post"]);	

	foreach ($post as $document)
	{
		$name = $document["name"];
		$date = $document["date"];
		$post = $document["msg"];
		print "<p class=\"thread_post\"><b>".$name." on ".$date."</b> :: ".$post."</p>\n";
	}
}


function pollListResponses($id)
{
	// valling shared variables
	global $collection_thread;

	$cursor = $collection_thread->findOne(array('_id' => new MongoID($id)));	

	$responses = $cursor["responses"];
	print_r($responses);
	foreach($responses as $response)
	{
		//print $response;
		//print $response["name"]." voted ".$response["choice"];		
	}		
}

function pollQuestion($id)
{
	// callin shared variables
	global $collection_thread;

	// return the poll
	$cursor = $collection_thread->findOne(array('_id' => new MongoID($id)));
	
	print "<p class=\"poll_question\">".$cursor["question"]."</p>";
	
	foreach ($cursor["options"] as $option)
	{
		print "<input type=\"radio\" name=\"choice\" value=\"".$option."\">".$option."<br>";
		//print "<br>".$option.$cursor;
	}	
}

function pollNew($mcatagory, $scatagory, $topic, $question, $options)
{

        // callin shared variable
        global $collection_thread;

        // validate all data
        field_validator("Poll", $topic, "alphanumeric_space", 2, 50);

	// timestamp to detect new posts
        $timestamp=round(microtime(true) * 100);

        // start a new thread
        $document = array(
                "poll" => true,
                "maincatagory" => $mcatagory,
                "subcatagory" => $scatagory,
                "topic" => $topic,
                "question" => $question,
		"options" => $options,
		"timestamp" => $timestamp
        );

        $collection_thread->insert($document);
}


function userList()
{
        // callin shared variables
        global $collection_auth;

	// find all users
        $cursor = $collection_auth->find();

	// print table of results

	Print "<table border cellpadding=3>";
	Print "<tr><th>Login</th><th>Name</th></tr>";
        foreach($cursor as $document)
        {
                Print "<td>".$document['email']."</td>";
                Print "<td>".$document['firstname']." ".$document['lastname']."</td>";

                Print "<td><form action=\"account_edit.php\" method=\"post\">";
                Print "<input type=\"hidden\" name=\"id\" value=\"".$document['_id']."\">";
                Print "<input type=\"submit\" value=\"Edit\"></form></td>";

                Print "<td><form action=\"account_permission.php\" method=\"post\">";
                Print "<input type=\"hidden\" name=\"id\" value=\"".$document['_id']."\">";
                Print "<input type=\"submit\" value=\"Permission\"></form></td>";

                Print "<td><form action=".$_SERVER["PHP_SELF"]." method=\"post\">";
                Print "<input type=\"hidden\" name=\"id\" value=\"".$document['_id']."\">";
                Print "<input type=\"submit\" value=\"Delete\"></form></td>";

                Print "</tr>";
        }
        Print "</table>";
}


function checkPass($login, $password) {

        // callin shared variable
        global $collection_auth;

	// check its a proper email > 6 characters
	field_validator("login name", $login, "email", 6, 50);
        
	// check the password is >4 <=3 characters
	field_validator("password", $password, "string", 4, 30);

	// return the users password hash
	$document = $collection_auth->findOne(array("email" => $login));

	// hash our password as passwords are saved as hash
	$hash=md5($password);

	// Check the password matches
	if($document["hash"]==$hash) {

		//set the session variables uses in the other scripts
		$_SESSION["email"]=$document["email"];
		$_SESSION["name"]=$document["firstname"]." ".$document["lastname"];
		$_SESSION["loggedIn"]=true;
		return true;
	}
	//Bad Login:
	return false;
}



function authGroupCheck($group) {
        
        // callin shared variable
        global $collection_auth;

	// check its a proper email > 6 characters
	field_validator("Security Group", $group, "alphanumeric", 6, 50);

	$email=$_SESSION["email"];

	$document = $collection_auth->findOne(array("email" => $email, "permission" => $group));

	if (! $document)
	{ return false; }
	else { return true; }
} 

function authGroupRedirect($group)
{
	if (!authGroupCheck($group)){ header("Location: _deny.php");}
}

function flushMemberSession() {

	// use unset to destroy the session variables
	unset($_SESSION["email"]);
	unset($_SESSION["name"]);
	unset($_SESSION["loggedIn"]);

	// session_destroy to destroy all data associated
	session_destroy();
}



// function validates HTML form field data passed to it:

function field_validator($field_descr, $field_data,
  $field_type, $min_length="", $max_length="",
  $field_required=1) {
	/*
	Field validator:
	This is a handy function for validating the data passed to
	us from a user's <form> fields.

	Using this function we can check a certain type of data was
	passed to us (email, digit, number, etc) and that the data
	was of a certain length.
	*/
	# array for storing error messages
	global $messages;

	# first, if no data and field is not required, just return now:
	if(!$field_data && !$field_required){ return; }

	# initialize a flag variable - used to flag whether data is valid or not
	$field_ok=false;

	# this is the regexp for email validation:
	$email_regexp="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|";
	$email_regexp.="(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

	# a hash array of "types of data" pointing to "regexps" used to validate the data:
	$data_types=array(
		"email"=>$email_regexp,
		"digit"=>"^[0-9]$",
		"number"=>"^[0-9]+$",
		"alpha"=>"^[a-zA-Z]+$",
		"alpha_space"=>"^[a-zA-Z ]+$",
		"alphanumeric"=>"^[a-zA-Z0-9]+$",
		"alphanumeric_space"=>"^[a-zA-Z0-9 ]+$",
		"string"=>""
	);

	# check for required fields
	if ($field_required && empty($field_data)) {
		$messages[] = "$field_descr is a required field.";
		return;
	}

	# if field type is a string, no need to check regexp:
	if ($field_type == "string") {
		$field_ok = true;
	} else {
		# Check the field data against the regexp pattern:
		$field_ok = ereg($data_types[$field_type], $field_data);
	}

	# if field data is bad, add message:
	if (!$field_ok) {
		$messages[] = "Please enter a valid $field_descr.";
		return;
	}

	# field data min length checking:
	if ($field_ok && ($min_length > 0)) {
		if (strlen($field_data) < $min_length) {
			$messages[] = "$field_descr is invalid, it should be at least $min_length character(s).";
			return;
		}
	}

	# field data max length checking:
	if ($field_ok && ($max_length > 0)) {
		if (strlen($field_data) > $max_length) {
			$messages[] = "$field_descr is invalid, it should be less than $max_length characters.";
			return;
		}
	}
}
?>
