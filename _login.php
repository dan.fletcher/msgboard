<?php

require_once("_config.php");

// redirect to main page if user is already logged in.
if ($_SESSION["loggedIn"]){
header("Location: index.php");
}

// check if this is a submission
if(isset($_POST["submit"])) {


	if($messages){
		doIndex();
		// note we have to explicity 'exit' from the script, otherwise
		// the lines below will be processed:
		exit;
	}

	// OK if we got this far the form field data was of the right format;
	// now check the user/pass pair match those stored in the db:
	/*
	If checkPass() is successful (ie the login and password are ok),
	then $row contains an array of data containing the login name and
	password of the user.
	If checkPass() is unsuccessful however, $row will simply contain
	the value 'false' - and so in that case an error message is
	stored in the $messages array which will be displayed to the user.
	*/

	if( !checkPass($_POST["login"], $_POST["password"]) ) {
		// login/passwd string not correct, create an error message:
        	$messages[]="Incorrect login/password, try again";
	}

	/*
	If there are error $messages, errors were found in validating form data above.
	Call the 'doIndex()' function (which displays the login form) and exit.
	*/
	if($messages){
		doIndex();
		exit;
	}

	/*
	If we got to this point, there were no errors - start a session using the info
	returned from the db:
	*/
	
 //       cleanMemberSession($row["Firstname"], $row["User_ID"], "alpha", 0 , 30);

	// and finally forward user to members page (populating the session id in the URL):
	header("Location: index.php");
} else {
	// The login form wasn't filled out yet, display the login form for the user to fill in:
	doIndex();
}

/*
This function displays the default 'index' page for this script.  This consists of just a simple
login form for the user to submit their username and password.
*/
function doIndex() {
	/*
	Import the global $messages array.
	If any errors were detected above, they will be stored in the $messages array:
	*/
	global $messages;

	/*
	also import the $title for the page - note you can normally just declare all globals on one line
	- ie:
	global $messages, $title;
	*/
	global $title;

	// drop out of PHP mode to display the plain HTML:
?>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" type="text/css" href="_styles.css">

<body>
<?php
// if there are any messages stored in the $messages array, call the displayErrors
// function to output them to the browser:
if($messages) { displayErrors($messages); }

/*
PHP_SELF:
The $_SERVER superglobals variable $PHP_SELF is one of the most useful predefined variables in PHP.
It contains the URI (uniform resource indicator) of the current script.
For example if this script is at http://example.com/somedir/join.php, then $_SERVER["PHP_SELF"] will contain:
/somedir/join.php

This is very useful because it means if you change the name of the script, you don't have to change every reference
to the script in <form> tags - $_SERVER["PHP_SELF"] automatically require_onces the current script URI!
*/
?>
<form action="<?php print $_SERVER["PHP_SELF"]; ?>" method="POST">
<table>
<tr><td>Login:</td><td><input type="text" name="login"
value="<?php print isset($_POST["login"]) ? $_POST["login"] : "" ; ?>"
maxlength="50"></td></tr>
<tr><td>Password:</td><td><input type="password" name="password" value="" maxlength="30"></td></tr>
<tr><td>&nbsp;</td><td><input name="submit" type="submit" value="Submit"></td></tr>
</table>
</form>
</body>
</html>
<?php
}
?>
