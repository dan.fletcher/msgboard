<?php


//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);


$upload_dir = "../msgboard/upload/";


include_once("_functions.php");


/*
  Register session variables

  We register our session variables here to save us retyping the
  session variable registration code in every script that requires
  the session variables (which is every script!)
*/

session_start();
$_SESSION["email"];
$_SESSION["name"];
$_SESSION["loggedIn"];
#$_SESSION["groups"];

/*
  Initialize global variables

  We declare a $messages array which we will use to hold error
  messages for display to users in case of errors.  Declaring this
  variable an array here saves us retyping it in every script that
  might use the $messages array (see above).
*/
$messages=array();

/*
  We initialize the following variables - $dbhost, $dbuser, $dbpass
  and $dbname - with the values correct for our mysql database.

  You should change these values to reflect your own database details:
*/

connectDB();

?>

